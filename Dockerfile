FROM nginx:1.18.0

RUN rm /etc/nginx/nginx.conf /etc/nginx/conf.d/default.conf

RUN mkdir /app

COPY ./app/ /app

COPY ./nginx.conf /etc/nginx

COPY ./myhost.conf /etc/nginx/conf.d/
